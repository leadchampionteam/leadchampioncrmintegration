# README #

L'obiettivo di questo progetto è fornire un'interfaccia per la creazione di librerie agnostiche di integrazione tra un qualsiasi CRM e Lead Champion.

### Uso ###
Verrà fornito il jar da aggiungere alle librerie del proprio progetto.
Nel caso in cui usiate Maven è possibile scaricare tale progetto e installarlo nel reposotori locale.
In tale caso si potrà aggiungere la dipendenza alla libreria che si sta creando aggiungendo le seguenti righe all'interno del tag dependencies all'interno per proprio file pom.xml

    <dependencies>
        <dependency>
            <groupId>com.leadchampion</groupId>
            <artifactId>LeadChampionCrmIntegration</artifactId>
            <version>1.20.x.y</version>
        </dependency>
    . . . 
    </dependencies>

Sarà quindi necessario implementare l'interfaccia LCCrmIntegrationDAO.

* Definire un costruttore con i parametri di cui si ha bisogno per l'autenticazione e le varie chiamate. Ad esempio (authEndpoint, clientId, clientSecret, refreshToken) nel caso di autenticazione OAuth 2.0
* Implementare i vari metodi di create, update, delete ecc.

NB: tenere conto che nell'interfaccia i metodi hanno parametri di tipo JSONObject che si riferiscono ale entità su cui operare. 

Tali oggetti sono popolati da pattern "chiave:valore" in base alla definizione dei modelli.
Definizioni che devono essere comunicate in fase di realizzazione dell'integrazione.

