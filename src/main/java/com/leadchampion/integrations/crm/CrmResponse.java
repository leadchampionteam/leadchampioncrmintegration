package com.leadchampion.integrations.crm;

import okhttp3.Response;
import org.codehaus.jettison.json.JSONException;

import java.io.IOException;

public class CrmResponse {
	private boolean success;
	private Integer responseCode;
	private String responseBody;

	public CrmResponse(boolean success, Integer responseCode, String body) {
		this.success = success;
		this.responseCode = responseCode;
		this.responseBody = body;
	}

	public CrmResponse(Response response) throws IOException, JSONException {
		this.success = response.isSuccessful();
		this.responseCode = response.code();
		this.responseBody = response.body().string();
	}

	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public Integer getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseBody() {
		return responseBody;
	}
	public void setResponseBody(String responseBody) {
		this.responseBody = responseBody;
	}

	@Override
	public String toString() {
		return "CrmResponse{" +
				"success=" + success +
				", responseCode=" + responseCode +
				", responseBody=" + responseBody +
				'}';
	}
}
