package com.leadchampion.integrations.crm;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

public interface LCCrmIntegrationDAO {
	// ENTITIES
	Long countCustomEntity(String entityName) throws Exception;

	CrmResponse deleteCustomEntity(String entityName, String entityID) throws Exception;

	CrmResponse createCustomEntity(String entityName, JSONObject entityJson) throws Exception;

	CrmResponse updateCustomEntity(String entityName, String entityID, JSONObject entityJson) throws Exception;

	JSONArray getCustomEntities(String entityName) throws Exception;

	JSONArray getCustomEntitiesFiltered(String entityName, CrmFilters filter) throws Exception;

	JSONArray getEntitiesList() throws Exception;
}
