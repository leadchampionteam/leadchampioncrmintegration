package com.leadchampion.integrations.crm;

public abstract class CrmFilters {
	public enum Condition {
		EQUALS,
		CONTAINS,
		GREATER_THAN,
		LOWER_THAN;
		// altre cose che ci possono esere utili
	}

	public void addFilter(String fieldName, Object fieldValue) {
		addFilter(fieldName, fieldValue, Condition.EQUALS);
	}

	public abstract void addFilter(String fieldName, Object fieldValue, Condition condition);
}
